# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
#GUESSING GAME#


def guessing_game
  answer, guesses = 1 + rand(99), []
  p "guess a number"
  until guesses.last == answer
    guess = gets.chomp.to_i
    guesses << guess
    if guesses.last > answer
      p "#{guess} is too high"
    elsif guesses.last < answer
      p "#{guess} is too low"
    else
      p "#{guess} is correct! It took you #{guesses.length} guesses"
    end
  end
end

def file_shuffler
 p "Enter a file name:"
 file_name = gets.chomp
 lines = File.readlines(file_name)
 lines_shuffled = lines.shuffle

 File.open("#{file_name}-shuffled.txt", "w") do |f|
   lines_shuffled.each {|line| f.puts line}
 end
end
